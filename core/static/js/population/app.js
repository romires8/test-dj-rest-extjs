Ext.require('Ext.container.Viewport');

Ext.application({
    requires: ['Ext.container.Viewport', 'Ext.util.Cookies'],
    name: 'PPN',

    appFolder: 'static/js/population/app',

    controllers: [
        'Cities'
    ],
    launch: function() {
        // Set token
        var csrftoken  = Ext.util.Cookies.get("csrftoken");
        if(csrftoken) {
            Ext.Ajax.defaultHeaders = Ext.apply(Ext.Ajax.defaultHeaders || {}, {
                'X-CSRFToken': csrftoken
            });
        }

        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: {
                xtype: 'citylist'
            }
        });
    }
});