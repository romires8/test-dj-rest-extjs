Ext.define('PPN.controller.Cities', {
    extend: 'Ext.app.Controller',
    views: [
        'city.List',
        'city.Edit'
    ],

    stores: [
        'Cities',
        'Regions'
    ],

    models: ['City'],

    init: function() {
        var component = this;
        this.control({
            'citylist': {
                itemdblclick: this.editCity,
                itemcontextmenu: function(view, record, item, index, e) {
                    var r = record.data;
                    e.stopEvent();
                    if (!item.ctxMenu) {
                        item.ctxMenu = new Ext.menu.Menu({
                            items : [{
                                text: "Удалить",
                                icon: "/static/js/extjs/examples/restful/images/delete.png",
                                handler:function(btn) {
                                    Ext.Msg.confirm('Подтверждение удаления', 'Вы действительно хотите удалить запись?',
                                        function(btn, text) {
                                            if(btn == 'yes') {
                                                component.deleteCity(view, index);
                                            }
                                        }
                                    );
                                }
                            }],
                            defaultAlign: "tl"
                        });
                    }
                    item.ctxMenu.showBy(item);
                }
            },
            'citylist button[action=edit]': {
                click: this.editCity
            },
            'cityedit button[action=save]': {
                click: this.saveCity
            },
            'citylist button[text="Добавить город"]': {
                click: this.addCity
            },
            'citylist dockedItems pagingtoolbar': {
                click: this.zzz
            }
            
        });
    },

    saveCity: function(button) {
        var win = button.up('window'),
        form = win.down('form'),
        record = form.getRecord(),
        values = form.getValues();

        // TODO: set valid value
        values['some_date'] = '2016-01-27';
        values['region'] = 'http://127.0.0.1:8000/api/regions/1?format=json'


        if(record) { // from Edit
            record.set(values);
        }
        else { // from Add
            this.getCitiesStore().add(values);
        }
        this.getCitiesStore().sync({
		    success: function(batch, options){
                win.close();
		    },
		    failure: function(batch, options) {
                alert('Error');
		    },
			scope: this
		});
    },
    addCity: function(button) {
        var view = Ext.widget('cityedit');
        view.setTitle('Добавление города');
    },
    deleteCity: function(grid, rowIndex) {
        this.getCitiesStore().removeAt(rowIndex);
        this.getCitiesStore().sync();
    },
    editCity: function(grid, record) {
        var view = Ext.widget('cityedit');
        view.down('form').loadRecord(record);
    }
});
