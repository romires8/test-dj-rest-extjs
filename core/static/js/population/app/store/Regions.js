Ext.define('PPN.store.Regions', {
    extend: 'Ext.data.Store',
    fields: ['name', 'url', 'id', 'slug'],
    model: 'PPN.model.Region',

    pageSize: 100,
    remoteSort: true,
    remoteFilter: true,
    autoLoad: {start: 0, limit: 100},

    proxy: {
        type: 'rest',
        url : '/api/regions',
        extraParams: {'format': 'json'},
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'count'
        }
    }
});