Ext.define('PPN.store.Cities', {
    extend: 'Ext.data.Store',
    model: 'PPN.model.City',

    pageSize: 25,
    remoteSort: true,
    remoteFilter: true,
    autoLoad: {start: 0, limit: 25},

    proxy: {
        type: 'rest',
        url : '/api/cities',
        extraParams: {'format': 'json'},
        simpleSortMode : true,
        reader: {
            type: 'json',
            root: 'results',
            totalProperty: 'count'
        }
    }
});
