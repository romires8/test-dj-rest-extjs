Ext.define('PPN.view.city.List' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.citylist',

    title: 'Города России',

    store: 'Cities',

    tools: [
        {type: 'help', 
            handler: function(event, toolEl, panel){
                alert('Help');
            }
        },
        {type: 'gear'}
    ],    

    initComponent: function() {
        
        var component = this;

        this.columns = [
            {header: 'Название', dataIndex: 'name', flex: 1},
            {header: 'Население в 2014', dataIndex: 'population2014', 
                renderer : function(val) {
                    if (val > 1000000) { // города более милиона жителей
                        return '<span style="color: orange;font-weight: bold;">' + val + '</span>';
                    } else {
                        return '<span style="color: darkgreen;">' + val + '</span>';
                    }
                    return val;
                }, flex: 1
            },
            {header: 'Дата', dataIndex: 'some_date', xtype:'datecolumn', format:'d M Y', flex: 1},
            {header: 'Регион', dataIndex: 'region', flex: 1},
            {header: '', width:50,
                xtype:'actioncolumn',
                name:'removeActionCol',
                items: [{
                    icon: '/static/js/extjs/examples/restful/images/delete.png',
                    tooltip: 'Удалить',
                    // В controller.Cities также есть пример удаления через контекстное меню
                    handler: function(grid, rowIndex, colIndex) {
                        var rec = grid.getStore().getAt(rowIndex);
                        Ext.Msg.confirm('Подтверждение удаления', 'Вы действительно хотите удалить запись?',
                            function(btn, text){
                                if(btn == 'yes') {
                                    grid.getStore().removeAt(rowIndex);
                                    grid.getStore().sync();
                                }
                            }
                        );
                    }
                }], flex: 1
            }
        ];

        var toolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            items: [
                {
                    xtype    : 'textfield',
                    name     : 'field',
                    emptyText: 'Название города',
                    fieldLabel:'Город'
                }, 
                {
                    xtype: 'combobox',
                    fieldLabel: 'Регион',
                    autoSelect:true,
                    store: 'Regions',
                    valueField:'id',
                    displayField:'name',
                    queryMode:'local',
                    editable: false
                },
                {
                    xtype: 'datefield',
                    fieldLabel: 'Выбрать дату',
                    format: 'Y-m-d',
                    maxValue: new Date(2018, 12, 31),
                    minValue: new Date(2000, 1, 1),
                    disabledDates: ['25/11/2012', '22/11/2012']
                },
                {
                    text: 'Найти',
                    handler: function () {
                        alert('Поиск...');
                    }
                },'-',
                {
                    text: 'Добавить город',
                    icon: '/static/js/extjs/examples/shared/icons/fam/add.png'
                }
            ]
        });
        this.dockedItems.push(toolbar);
    
        this.callParent(arguments);
    },

    dockedItems: [
        {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayMsg: '{0} - {1} of {2}',
            emptyMsg: 'No data to display',
            store: 'Cities',
            displayInfo: true
        }
    ]
});