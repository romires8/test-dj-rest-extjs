Ext.define('PPN.view.city.Edit', {
    extend: 'Ext.window.Window',
    alias: 'widget.cityedit',

    title: 'Редактирование города',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [
        {
            xtype: 'form',
            items: [
            {
                xtype: 'textfield',
                name : 'name',
                fieldLabel: 'Название'
            },{
                xtype: 'textfield',
                name : 'population2014',
                fieldLabel: 'Население 2014'
            },{
                xtype: 'textfield',
                name : 'slug',
                fieldLabel: 'Slug'
            },{
                xtype: 'combobox',
                fieldLabel: 'Регион',
                store: 'Regions',
                name:'region',
                valueField:'url',
                displayField:'name',
                queryMode:'local',
                editable: false,
                forceSelection: true,
            },{
                xtype: 'datefield',
                name : 'some_date',
                fieldLabel: 'Дата',
                format: 'Y-m-d'
            }]
        }];

        this.buttons = [
            {
                text: 'Сохранить',
                action: 'save'
            },{
                text: 'Отмена',
                scope: this,
                handler: this.close
            }
        ];

    this.callParent(arguments);
    }
});