Ext.define('PPN.model.City', {
    extend: 'Ext.data.Model',
    fields: ['name', 'population2014', 'some_date', 'region', 'slug'],
    belongsTo: 'Region'
});