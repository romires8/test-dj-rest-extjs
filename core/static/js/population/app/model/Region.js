Ext.define('PPN.model.Region', {
  extend: 'Ext.data.Model',
  fields: ['name', 'slug', 'url'],
  associations: [{ type: 'hasMany', model: 'City' }]
});