# -*- coding: utf8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class NameSlugMixin(models.Model):
    """
    Абстракный класс: поля name и slug
    """
    name = models.CharField(_(u'Название'), max_length=255)
    slug = models.SlugField(_(u'Название для URL'), max_length=255, unique=True, allow_unicode=False)

    def __unicode__(self):
        return self.name

    class Meta:
        abstract = True


class Region(NameSlugMixin):
    """
    Регионы
    """
    class Meta:
        verbose_name = _(u"Регион")
        verbose_name_plural = _(u"Регионы")


class City(NameSlugMixin):
    """
    Город
    """
    region = models.ForeignKey(Region, verbose_name=_(u'Регион'), on_delete=models.PROTECT)
    population2014 = models.IntegerField(_(u'Население в 2014'), default=0)
    some_date = models.DateField(_(u'Какая-то дата'), null=True, blank=True)

    class Meta:
        verbose_name = _(u"Город")
        verbose_name_plural = _(u"Города")



