# -*- coding: utf8 -*-

from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, Http404, get_list_or_404, redirect
from rest_framework import serializers, viewsets
from rest_framework import filters
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination

from .models import Region, City


def index(request):
    """
    Стартовая страница
    """
    return render(request, "index.html", {'title': "Население России в 2014 гsоду"})



class StandardResultsSetPagination(PageNumberPagination):
    page_size = 25
    page_size_query_param = 'page_size'
    max_page_size = 1000


""" Регионы  """


class RegionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Region
        fields = ('url', 'id', 'name', 'slug')


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all().order_by('name')
    serializer_class = RegionSerializer



""" Города """


class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = City
        fields = ('url', 'id', 'name', 'slug', 'region', 'population2014', 'some_date')


class AdaptOrderingFilter(filters.OrderingFilter):
    """
    Адаптация параметра направления сортировки
    """
    def get_ordering(self, request, queryset, view):
        ordering = super(AdaptOrderingFilter, self).get_ordering(request, queryset, view)
        order_dir = request.query_params.get('dir')
        prefix = order_dir and order_dir == 'DESC' and '-' or ''
        if prefix and ordering:
            ordering = [prefix+field for field in ordering]
        return ordering


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filter_backends = (filters.DjangoFilterBackend, AdaptOrderingFilter, filters.SearchFilter)
    filter_fields = ('name', 'slug')
    search_fields = ('name',)
    pagination_class = StandardResultsSetPagination
    ordering_fields = '__all__'
    ordering = '-population2014'
